package demo;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;


public class MyServiceTest {

    private MyService underTest;

    private ExternalService externalService;

    @BeforeEach
    void setUp(){
        externalService = new ExternalService();
        underTest = new MyService(externalService);
    }

    @Test
    void itShouldValidateID(){
        String id = "10";

        Boolean reslut = underTest.validate(id);

        assertTrue(reslut);


    }

}
