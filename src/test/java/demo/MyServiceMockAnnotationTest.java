package demo;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class MyServiceMockAnnotationTest {

    private MyService underTest;

    @Mock
    private ExternalService externalService;

    @BeforeEach
    void setUp(){
        MockitoAnnotations.initMocks(this);
        underTest = new MyService(externalService);
    }

    @Test
    void itShouldValidateID(){
        String id = "10";

        when(externalService.getValidationData(id)).thenReturn("some data");


        Boolean result = underTest.validate(id);

        assertTrue(result);


    }
}
