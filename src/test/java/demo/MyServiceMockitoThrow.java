package demo;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

public class MyServiceMockitoThrow {

    @InjectMocks
    private MyService underTest;

    @Mock
    private ExternalService externalService;

    @BeforeEach
    void setUp(){

        MockitoAnnotations.initMocks(this);
    }


    @Test
    void itShouldThrowAnException(){
        String id = null;

        when(externalService.getValidationData(null)).thenThrow(RuntimeException.class);

        assertThrows(RuntimeException.class, ()-> underTest.validate(id));



    }
}
